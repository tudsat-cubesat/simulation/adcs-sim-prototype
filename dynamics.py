from pyquaternion import Quaternion
import numpy as np


def angular_vel_ode(J, w, M_list=None):
    """
    the angular velocity ode with external torques in M
    """
    if M_list is not None:
        M_total = np.sum(M_list, axis=0)
    else:
        M_total = np.array([0, 0, 0])
    
    J_inv = np.linalg.inv(J)
    return (-np.matmul(J_inv, np.cross(w, np.matmul(J, w))) +
            np.matmul(J_inv, M_total))