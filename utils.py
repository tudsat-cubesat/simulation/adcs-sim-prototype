from astropy import coordinates as coord, units as u
from astropy.time import Time
import numpy as np
import pandas as pd
import pickle
from pyquaternion import Quaternion
import sensors, actuators

def TEME_to_geodetic(x, y, z, t):
    """
    convert coordinates(km) given in TEME frame and a time(julian day) 
    to latitude(deg), longitude(deg) and height(km)
    """
    t = Time(t, format="jd")
    # convert from the ECI frame TEME to ITRS which is an ECEF frame
    teme = coord.builtin_frames.TEME(x*u.km, y*u.km, z*u.km, 
                                     obstime=t, representation_type='cartesian')
    itrs = teme.transform_to(coord.ITRS(obstime=t)).cartesian
    # ITRS is the default frame for EarthLocation which enables a geodetic representation
    # the WGS84 should be used since it is also used by the magnetic model 
    earth_loc = coord.EarthLocation(x=itrs.x, y=itrs.y, 
                                    z=itrs.z).to_geodetic(ellipsoid="WGS84")
    lat = earth_loc.lat.degree
    lon = earth_loc.lon.degree
    height = earth_loc.height.to_value(u.km)
    return [lat, lon, height]

def ECEF_to_TEME(x, y, z, t):
    """
    convert the coordinates given in an ECEF to the TEME frame at
    the given time in julian days
    """
    t = Time(t, format="jd")
    ecef = coord.ITRS(x, y, z, obstime=t)
    teme = ecef.transform_to(coord.builtin_frames.TEME(obstime=t))
    return np.array(teme.cartesian.xyz)


def julian_to_decimal(time):
    """converts a julian day date to a decimal year"""
    t = Time(time, format="jd")
    return float(t.to_value("decimalyear"))

def julian_to_gps(time):
    """converts a julian day fractional to gps time"""
    t = Time(time, format="jd")
    return float(t.to_value("gps"))

def step_to_sensor(data, n_steps):
    """
    convert the data which is an array of dictionaries into a dicitonary of arrays.
    in the array of dictionaries the array is a list of timesteps with the dictionary
    assigning each sensor a measurement.
    in the dictionary of arrays the dictionary assigns each sensor an array of measurements.
    """
    sensors = {}
    names = data[0].keys()
    for name in names:
        #intialize the arrays for each sensor
        sensors[name] = np.empty((n_steps, len(data[0][name])))
        for i in range(n_steps):
            sensors[name][i] = data[i][name]
    return sensors

def get_attitude(vector1, vector2):
    """
    returns the quaternion that rotates one vector into the other.
    vectors are unitized after input
    """
    if np.array(vector1).all() == np.array(vector2).all():
        return Quaternion([1,0,0,0])
    unit_vector1 = vector1 / np.linalg.norm(vector1)
    unit_vector2 = vector2 / np.linalg.norm(vector2)
    rotation_axis = np.cross(unit_vector1, unit_vector2)
    rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))
    #the length of the vector represents the angle of rotation
    quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
    return quaternion

def make_sensors(config):
    """
    returns a dicitionary of sensors that are specified in the config
    """
    def get_orientation(orientation, name):
        #a dictionary connecting the names of the axes to their vectors
        vectors = {"+X": [1, 0,0],
                   "-X": [-1,0,0],
                   "+Y": [0,1,0],
                   "-Y": [0,-1,0],
                   "+Z": [0,0,1],
                   "-Z": [0,0,-1],}
        if "," in orientation:
            vec = read_config_vector(orientation)
            return vec / np.linalg.norm(vec)
        elif orientation in vectors.keys():
            return np.array(vectors[orientation])
        else:
            raise Exception("the orientation of sensor {} is not correct".format(name))
    
    sensor_dict = {}
    n_steps = int(int(config["simulation"]["duration"]) \
                / float(config["simulation"]["delta_t"])) + 1
    #magnetometers first:
    for i in range(int(config["sensors"]["n_magnetometers"])):
        # the name of the subsection that specifies each magnetometer
        identifier = "magnetometer" + str(i+1)
        subsection = config[identifier]
        #the _mag is added to distinguish magnetometers from other sensors
        name = subsection["name"] + "_mag"
        rel_position = read_config_vector(subsection["position"])
        orientation = get_orientation(subsection["orientation"], name)
        resolution = float(subsection["resolution"])
        sensor_dict[name] = sensors.Magnetometer(name, rel_position, orientation, 
                                                 resolution, n_steps)
    
    #photodiodes:
    for i in range(int(config["sensors"]["n_photodiodes"])):
        identifier = "photodiode" + str(i+1)
        subsection = config[identifier]
        name = subsection["name"] + "_photo"
        side = get_orientation(subsection["side"], name)
        normal = read_config_vector(subsection["normal"])
        FOV = float(subsection["FOV"])
        max_current = float(subsection["max_current"])
        noise = float(subsection["noise"])
        sensor_dict[name] = sensors.Photodiode(name, side, normal, noise, 
                                               max_current, FOV, n_steps)
    
    return sensor_dict

def make_actuators(config):
    """
    returns a dicitionary of actuators that are specified in the config
    """
    n_steps = int(int(config["simulation"]["duration"]) \
                / float(config["simulation"]["delta_t"])) + 1
    actuator_dict = {}
    for i in range(int(config["actuators"]["n_magnetorquers"])):
        identifier = "magnetorquer" + str(i+1)
        subsection = config[identifier]
        #TODO. add some sort of type attribute to sensors and actuators
        name = subsection["name"] + "_mgt"
        rel_position = read_config_vector(subsection["rel_position"])
        normal = read_config_vector(subsection["normal"])
        normal = normal / np.linalg.norm(normal)
        area = float(subsection["area"])
        n_windings = int(subsection["n_windings"])
        max_current = float(subsection["max_current"])
        noise = float(subsection["noise"])
        actuator_dict[name] = actuators.Magnetorquer(name, rel_position, normal, area,
                                                    n_windings, max_current, noise, n_steps)
    return actuator_dict

def read_config_vector(string):
    """
    convert the vector in the string into a numpy array
    """
    vector = string.split(",")
    return np.array(list(map(float, vector)))

def read_config_quaternion(string):
    """
    convert the vector in the string into a quaternion
    """
    quat = string.split(",")
    return Quaternion(list(map(float, quat)))

def dipole_field(position, moment):
    """
    the magnetic field vector in nT outside a dipole given a position relative to the dipole.
    position and moment as numpy array
    """
    #vacuum permeability
    mu_0 = 1.256637062*1e-6
    pos_norm = np.linalg.norm(position)
    pos_unit = position / pos_norm
    field = (mu_0/(4*np.pi)) * ((3 * pos_unit * np.dot(moment, pos_unit) - moment) / pos_norm**3)
    #convert to nT
    return field * 1e9

def saving(logs, file_name):
    """
    saves the logs to a file with given name
    """
    with open(file_name + ".pkl", "wb") as file:
        pickle.dump(logs, file)

import numpy as np
np.shape(np.array([[ 0.34805421,  0.64264481, -0.191111  ,  0.65524233],
       [ 0.34588385,  0.64154918, -0.19347431,  0.65676989],
       [ 0.33916503,  0.63855114, -0.2005293 ,  0.66106545]]))