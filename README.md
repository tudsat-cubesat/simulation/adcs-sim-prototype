# adcs sim prototype

A working prototype for the adcs simulation. Meant to simulate an environment for an attitude controller. 
Some of the code is from: https://github.com/gavincmartin/adcs-simulation  
reference for many models: Fundamentals of Spacecraft Attitude Determination and Control by F. Landis MarkleyJohn, L. Crassidis

## Current version includes:
* attitude kinematic and dynamic propagation
* orbit propagation using sgp4
* magnetic field model: World Magnetic Model 2020
* senors: magnetometer and light diodes
* actuators: magnetorquer
* simulation of perturbation torques from the environment:
    * Gravity gradient torque, solar radiation torque, aerodynamic torque, magnetic torque
* a simple controller that gets an attitude and can directly apply torque
* a controller that gets sensor readings and can control the current in magnetorquers
* rudimentary visualization of simulation results

this current version can be used to implement different controller models and test their effectivenes. 

## future versions should include:
* IMU sensor
* simulate failures of sensors and actuators

## How to use:
* windows users: get a C compiler (e.g. mingw64) and CMake and add it to your path, needed for the magnetic model
* install the libraries in requirements.txt, install with `$ pip install -r requirements.txt`
* download all the files in this repository
* opional: change the simulation parameter in sim.conf
* run simulation with `$ python main.py`

