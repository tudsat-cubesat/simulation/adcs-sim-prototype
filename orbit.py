from sgp4.api import jday, Satrec, WGS72
from sgp4.conveniences import sat_epoch_datetime
import math, datetime, utils

def setup_orbit(config):
    """
    returns the sgp4 satellite object with orbit specified in config
    """
    sgp4_sat = Satrec()
    sgp4_sat.sgp4init(WGS72, "i", 
                666,                                                       #satellite number
                compute_epoch(config),                                     #epoch (days since 1949 December 31)
                float(config["orbital_elements"]["b_star"]) / 6371,        #bstar (/earth_radii)
                float(config["orbital_elements"]["n_dot"]),                #ndot (revs/day)
                float(config["orbital_elements"]["nd_dot"]),               #nddot (revs/day^3)
                float(config["orbital_elements"]["ecco"]),                 #ecco
                float(config["orbital_elements"]["argpo"]),                #argpo (radians)
                float(config["orbital_elements"]["inclo"]) * math.pi/180,  #inclo (radians)
                float(config["orbital_elements"]["mo"]),                   #mo (radians)
                float(config["orbital_elements"]["no_kozai"]),             #no_kozai (radians/minute)
                float(config["orbital_elements"]["nodeo"]) * math.pi/180,  #nodeo (radians)
                )

    return sgp4_sat

def pos_vel(sgp4_sat, jd, frac):
    """
    returns position and velocities in km and km/s in TEME frame at specified time
    """
    e, r, v = sgp4_sat.sgp4(jd, frac)
    # e is an error code
    if e != 0:
        # a dict of errors is in sgp4.api import SGP4_ERRORS
        raise Exception("something in the sgp4 propagation went wrong. check the date and orbital elements")
    return r, v

def julian_day_frac(config):
    """
    returns a tuple containing a julian day and the frac of that day 
    coressponding to the point in time at start_real_time
    """
    jd, frac = jday(*time_from_config(config))
    return (jd, frac)

def compute_epoch(config):
    """
    returns the number of seconds since 1949 December 31 00:00 UT relative to
    the start time specified in the config
    """
    time = datetime.datetime(*time_from_config(config), tzinfo=datetime.timezone.utc)
    epoch_seconds = (time - datetime.datetime(1949, 12, 31, tzinfo=datetime.timezone.utc)).total_seconds()
    epoch_days = epoch_seconds / (60*60*24)
    return epoch_days

def time_from_config(config):
    """
    converts the start_time string from the config into a tuple of
    year, month, day, hour, minute and seconds
    """
    real_time = config["simulation"]["start_time"]
    year, mon, day = map(int, real_time.split("T")[0].split("-"))
    hr, minute, sec = map(int, real_time.split("T")[1].split(":"))
    return (year, mon, day, hr, minute, sec)

def sim_time_to_real_time(t, delta_t, sgp4_sat):
    """
    returns the real time in julian date, frac that corresponds to the simulation time t
    """
    dt_curr_time = sat_epoch_datetime(sgp4_sat)
    dt_delta_t = datetime.timedelta(seconds=delta_t + t)
    dt_next_time = dt_curr_time + dt_delta_t
    jd, frac = jday(dt_next_time.year, dt_next_time.month, dt_next_time.day, 
                    dt_next_time.hour, dt_next_time.minute, dt_next_time.second)
    return jd, frac
