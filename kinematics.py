from pyquaternion import Quaternion
import numpy as np


def kin_attitude_ode(attitude, w):
    """
    the kinematic attitude ode
    quat: attitude quaternion
    w: angular velocity vector (rad/s)
    returns quaternion representing attitude as numpy array
    """
    w_quat = Quaternion([0, *w])
    q_dot =  0.5 * w_quat * attitude
    return np.array(q_dot.elements)