import numpy as np
from pyquaternion import Quaternion
"""
this script defines the behavior of the controller
the simulation calls the controller function every second
"""



def simple_controller(current_attitude, desired_attitude):
    """
    a simple controller that receives a noisy current attitude and a desired attitude.
    it computes a control torque that is to be applied to the satellite for the next second.
    the amount of noise is given by satellite.contr_noise.
    the desired attitude is specified in satellite.desired_attitude but also passed as an arg
    params:
    current_attitude(Quaternion obj): the attitude of the satellite with some noise
    desired_attitude(Quaternion obj): the desired attitude of the satellite
    returns:
    numpy array (3x1): torque in N*m
    """

    #this controller is mainly meant for testing

    return np.array([0,0,0])

def controller(measurements, actuators, position, velocity, time, desired_attitude):
    """
    this controller receives the sensor reading and can set the actuator behaviour.
    it is called at each time step. 
    the position, velocity and time are given as a substitute for GNSS
    params:
    measurements(dict): a dictionary of all measurements. key->name, value->measurement
    actuators(dict):  a dictionary of all onboard actuators. key->name, value->actuator obj
    position(3x1 nparray): the position in km in the TEME frame
    velocity(3x1 nparray): the velocity in km in the TEME frame
    time(float): the time as gps time
    desired_attitude(quaternion): the desired attitude
    returns:
    estimated_attitude(Quaternion): attitude estimated at this time step
    """
    for name in actuators:
        if "mgt" in name:
            actuators[name].set_current(0.1)

    estimated_attitude = Quaternion()
    return estimated_attitude