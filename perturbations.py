import numpy as np
import environment
from pyquaternion import Quaternion

def aerodynamic_torque(position, velocity, altitude, attitude, side_length, C_D):
    """
    this model of aerodynamic torque assumes a 1U cubesat and models each side of the
    satellite as a flat panel with area 0.01 m^2.
    equations and model from Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis, 3.3.6.3
    """
    d = side_length
    area = d*d
    def force_per_plate(normal, area, altitude, relative_vel_B, C_D):
        """
        the force acting on each plate which is defined by the outward normal vector on that plate
        """
        inclination = np.dot(normal, relative_vel_B) / np.linalg.norm(relative_vel_B)
        rho = environment.atmospheric_density(altitude)
        return -0.5 * rho * C_D * np.linalg.norm(relative_vel_B) * \
               relative_vel_B * area * max(inclination, 0)
    # earths angular velocity in rad/s
    w_earth = 0.000072921158553
    # relative velocity in the ECI frame, atmosphere is not stationary in ECI
    v_rel_ECI = velocity + np.cross([0,0, w_earth], position)
    # relative velocity in the body frame
    v_rel_B = attitude.rotate(v_rel_ECI)

    # normal vectors of each side of the cube
    plate_normals = [[1,0,0], [0,1,0], [0,0,1], [-1,0,0], [0,-1,0], [0,0,-1]]
    # the vector from the satellite center of mass to the plate center of mass
    plate_pos = [[d,0,0], [0,d,0], [0,0,d], [-d,0,0], [0,-d,0], [0,0,-d]]
    forces = [force_per_plate(i, area, altitude, v_rel_B, C_D) for i in plate_normals]
    torques = [np.cross(plate_pos[i], forces[i]) for i in range(len(plate_pos))]
    return np.sum(torques, axis=0)

def gravity_gradient_torque(position, attitude, inertia_tensor):
    """
    the gravity gradient torque acting on the satellite. with a spherically symmetric gravity field.
    equations and model from Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis, 3.3.6.1
    """
    #gravitational constant * mass of the earth aka standard gravitational parameter of the earth
    mu = 3.986004418e14
    nadir_ECI = -1* position / np.linalg.norm(position)
    nadir_body = attitude.rotate(nadir_ECI)
    #position now needs to be in meters
    pos_magnitude = np.linalg.norm(position) * 1000
    return (3*mu/(pos_magnitude**3)) * np.cross(nadir_body, np.dot(inertia_tensor, nadir_body))

def solar_radiation_torque(position, attitude, time, R_spec, R_diff, R_abs, side_length):
    """
    the torque produced by the sunlight reflecting off of the satellite. only direct sunlight
    is considered, no albedo.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis 3.3.6.4
    """
    # if the satellite is in shadow there isnt any pertubation torque from this source
    if environment.is_in_shadow(position, time):
        return np.array([0,0,0])
    
    d = side_length
    area = d*d
    if R_spec + R_diff + R_abs != 1:
        raise Exception("the values for the refelction coefficient do not add up to 1!")
    
    earth_to_sun = environment.earth_to_sun(time)
    sat_to_sun = np.array(earth_to_sun) - np.array(position)
    sat_to_sun_B = attitude.rotate(sat_to_sun)
    sat_to_sun_unit_B = sat_to_sun_B / np.linalg.norm(sat_to_sun_B)

    solar_pressure = environment.solar_radiation_pressure(position, time)

    # normal vectors of each side of the cube
    plate_normals = [[1,0,0], [0,1,0], [0,0,1], [-1,0,0], [0,-1,0], [0,0,-1]]
    # the vector from the satellite center of mass to the plate center of mass
    plate_pos = [[d,0,0], [0,d,0], [0,0,d], [-d,0,0], [0,-d,0], [0,0,-d]]
    def force_per_plate(normal, area, solar_press, sat_to_sun):
        normal = np.array(normal)
        sat_to_sun = np.array(sat_to_sun)
        dot = np.dot(normal, sat_to_sun)
        return -solar_press * area * (2 * (R_diff/3 + R_spec * dot) * normal\
               + (1-R_spec) * sat_to_sun) * np.max(dot, 0)
    
    forces = [force_per_plate(i, area, solar_pressure, sat_to_sun_unit_B) for i in plate_normals]
    torques = [np.cross(plate_pos[i], forces[i]) for i in range(len(plate_pos))]
    return np.sum(torques, axis=0)

def magnetic_torque(position, dipole, time, attitude):
    """
    the torque acting on the satellite due to the magnetic field and
    the dipole of the satellite.
    """
    #the WMM model returns the magnetic field in nT
    field = np.array(environment.magnetic_field(position, time)) * 1e-9
    field_B = attitude.rotate(field)
    return np.cross(dipole, field_B)