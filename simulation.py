from scipy.integrate import ode
from pyquaternion import Quaternion
from kinematics import kin_attitude_ode
from dynamics import angular_vel_ode
from satellite import Satellite
from orbit import pos_vel, sim_time_to_real_time
from tqdm import tqdm
import numpy as np, utils, controller
"""
quaternion convention of pyquaternion: scalar first entry
"""


def simulate_adcs(satellite, sgp4_sat, start_real_time, start_sim_time=0, delta_t=1, stop_sim_time=6000):
    """
    the actual simulation function. sets up the propagator and records the data.
    the recorded data is returned as a dictionary.
    satellite(obj): a satellite object from this project
    sgp4_sat(obj): a satellite object for the sgp4 propagation
    start_real_time(2x1 tuple): contains the julian date and a fraction on that day
    start_sim_time(float): the start of the simulation time in seconds, relative to the real start time
    delta_t(float): time between recorded simulation steps in seconds
    stop_sim_time(float): number of seconds since the start_sim_time at which the sim will terminate

    returns(dictionary): logs of all values
    """
    solver = ode(dervatives_func)
    solver.set_integrator(
        'lsoda',
        rtol=(1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-10, 1e-6, 1e-6,
              1e-6),
        atol=(1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-12, 1e-8, 1e-8,
              1e-8),
        nsteps=10000)
    init_state = np.array([*satellite.attitude, *satellite.w])
    solver.set_initial_value(y=init_state, t=start_sim_time)
    

    #setup logging
    n_steps = int((stop_sim_time - start_sim_time) / delta_t) + 1
    #simulation data logging
    times = np.empty(n_steps)
    real_times = np.empty(n_steps)
    #satellite data logging
    attitudes = np.empty((n_steps, 4))
    ang_vels = np.empty((n_steps, 3))
    positions = np.empty((n_steps, 3)) #position in km, TEME frame
    velocities = np.empty((n_steps, 3)) #velocity in km/s, TEME frame
    #sensor logging
    sensor_measurements = np.empty(n_steps, dict)
    #actuator logging
    actuator_controls = np.empty(n_steps, dict)
    #perturbation logging
    atmospheric_torques = np.empty((n_steps, 3))
    gravity_torques = np.empty((n_steps, 3))
    solar_torques = np.empty((n_steps, 3))
    magnetic_torques = np.empty((n_steps, 3))

    #setup perturbation noise
    pert_noise = np.random.normal(0, satellite.perturbation_noise, (n_steps+1,3))

    #logging of the estimated
    estimated_attitude = np.empty((n_steps, 4))

    #setup progress bar
    pbar = tqdm(total=n_steps)

    i=0
    solver.set_f_params(satellite, pert_noise)
    while solver.successful() and solver.t <= stop_sim_time:

        #position and velocity calucaltions, refer to orbit.py file
        jd, frac = sim_time_to_real_time(solver.t, delta_t, sgp4_sat)
        satellite.real_time = jd + frac
        r, v = pos_vel(sgp4_sat, jd, frac)
        satellite.r = r
        satellite.v = v

        satellite.sim_time = solver.t

        #logging
        times[i] = solver.t
        real_times[i] = jd + frac
        attitudes[i] = solver.y[0:4]
        ang_vels[i] = solver.y[4:7]
        positions[i] = r
        velocities[i] = v
        #logging perturbations, these calculations are only for the logging
        #the actual calculations that influence the satellite are in derivatives_func
        atmospheric_torques[i] = satellite.compute_aerodynamic_torque()
        gravity_torques[i] = satellite.compute_gravity_gradient_torque()
        solar_torques[i] = satellite.compute_solar_radiation_torque()
        magnetic_torques[i] = satellite.compute_magnetic_torque()
        #logging the sensor measurements. a dictionary with key-> sensor name
        #and value-> measurement is saved at each time step
        sensor_measurements[i] = satellite.get_sensor_measurements()

        #calling the controller
        #TODO: is this the best place to call the controller?
        estimated_attitude[i] = controller.controller(sensor_measurements[i], satellite.actuators, r, v,
                                        utils.julian_to_gps(jd+frac), satellite.desired_attitude).elements
        
        #logging actuators controls, i.e. the controlling input of the actuator
        actuator_controls[i] = satellite.get_actuator_controls()


        #update progress bar
        pbar.update(1)
        #continue intergation
        i += 1
        solver.integrate(solver.t + delta_t)
    
    pbar.close()
    #convert the inconvenient data structure of the sensor measurements 
    #into a dictionary keys->sensor names, values->measurments of that sensor
    sensor_measurements = utils.step_to_sensor(sensor_measurements, n_steps)
    actuator_controls = utils.step_to_sensor(actuator_controls, n_steps)
    logs = {"times":times, "real_times": real_times, "attitudes":attitudes, 
            "angular_velocities": ang_vels, "positions":positions, "velocities":velocities,
            "atmospheric_torque": atmospheric_torques, "gravity_torque": gravity_torques,
            "solar_torque": solar_torques, "magnetic_torque": magnetic_torques,
            "random_torque": pert_noise[:n_steps],
            "sensor_measurements": sensor_measurements, 
            "actuator_controls": actuator_controls,
            "estimated_attitudes": estimated_attitude}
    return logs


def dervatives_func(t, state, satellite, perturbation_noise):
    """
    the combination of the kinematic attitude ode and the angular velocity ode
    t: time (s)
    state: combined state conatining the attitude quaternion and the angular velocity
           state[0:4]: attitude 
           state[4:7]: angular velocity
           (numpy array 7*1)
    satellite: (satellite obj)
    """
    satellite.attitude = Quaternion(state[:4]).normalised
    satellite.w = state[4:7]
    del_state = np.zeros((7,))
    del_state[0:4] = kin_attitude_ode(satellite.attitude, satellite.w)
    
    #pass some noise as a perturnation torque
    noise_torque = perturbation_noise[int(t)]
    pertubation_torques = satellite.compute_pertubation_torques()
    #call the controller
    noisy_att = np.random.normal(satellite.attitude.elements, satellite.contr_noise)
    noisy_att = Quaternion(noisy_att).normalised
    contr_torque = controller.simple_controller(noisy_att, satellite.desired_attitude)
    #FLAG: add all control or perturbation toruqes here
    del_state[4:7] = angular_vel_ode(satellite.J, satellite.w, 
                                    [noise_torque, pertubation_torques, contr_torque])

    return del_state