import numpy as np

class Magnetorquer(object):

    def __init__(self, name, rel_position, normal, area, 
                n_windings, max_current, noise, n_steps):
        """
        name(string): a unique name for each magnetorquer
        rel_position(3x1): the position in the satellite in meter
        normal(3x1): the normal vector on the currrent loop, which is also
                     the direction of the magnetic moment
        area(float): the area encompassed by the current loop in m^2
        n_windings(int): the number of windings in the loop
        max_current(float): the maximum amount of current that can be applied to
                            the magnetorquer in Amperes
        noise(float): the sigma of the noise level of the current in Amperes
        n_steps(int): number of user defined integration steps
        """
        self.name = name
        self.rel_postion = rel_position
        self.normal = normal
        self.area = area
        self.n_windings = n_windings
        self.max_current = max_current
        self.noise = noise
        self.n_steps = n_steps

        self.current = 0
        self.noise_cache = self.make_noise_cache(1)
    
    def dipole_moment(self, satellite):
        #apply noise to the current
        return self.n_windings * self.area * self.normal * \
              (self.current + self.noise_cache[int(satellite.sim_time)])
    
    def set_current(self, current):
        self.current = min(self.max_current, current)
    
    def get_control(self, satellite):
        return np.array([self.current+ self.noise_cache[int(satellite.sim_time)]])
    
    def make_noise_cache(self, dimensions):
        """
        caching the noise values is necessary because the integrator would get confused
        otherwise. between the user defined steps it does smaller steps to ensure 
        the accuracy of the results. without caching the sensors would change its
        mesurement with each small step slightly which would slightly alter the 
        applied control torque. this would prevent the integrator from staying
        within an acceptable error for the ode solutions.
        """
        return np.random.normal(0, self.noise, (self.n_steps, dimensions))