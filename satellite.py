"""
Satellite module that contains the satellite class
TEME is very close to J2000 (difference only a few milliarcesonds)
"""

import perturbations, utils, environment
import numpy as np
from pyquaternion import Quaternion

class Satellite(object):


    def __init__(self, J, w, dipole, side_length, attitude, desired_attitude, 
                 C_D, R_spec, R_diff, R_abs, sensors, actuators,
                 perturbation_noise=0.1, contr_noise=0.1):
        """
        J(3x3 array): Inertia tensor
        w(3x1 array): angular velocity
        dipole(3x1 array): magnetic dipole moment
        side_length(float): length of the side of the satellite, which is assumed to be a cube
        att(Quaternion): Quaternion representation of the attitude(TEME frame to body frame)
        desired_attitude(Quaternion): the attitude that the controller will try to attain
        perturbation_noise(float): abs value for sigma of the noise
        contr_noise(float): relative value for sigma of the noise for the attitude
                            passed to the controller and the torque returned by the controller
        C_D(float): drag coefficient
        R_spec(flaot): specular reflection coefficient
        R_diff(float): diffuse reflection coefficient
        R_abs(float): absorption coefficient
        sensors(dictionary): a dictionary of all onboard sensors. key->name, value->sensor obj
        actuators(dictionary): a dictionary of all onboard actuators. key->name, value->actuator obj
        
        other attributes:
            real_time(float): the real time in julian days
            sim_time(float): the simulation time in seconds
            r(3x1 array): position vector in km (TEME frame)
            v(3x1 array): velocity vector in km/s (TEME frame)
            diag_J(boolean): wether the inertia tensor J is diagonal
        """
        self.real_time = 0
        self.sim_time = 0
        self.J = J
        self.w = w
        self.residual_dipole = dipole
        self.side_length = side_length
        self.r = np.empty(3)
        self.v = np.empty(3)
        self.attitude = attitude
        self.C_D = C_D
        self.R_spec = R_spec
        self.R_diff = R_diff
        self.R_abs = R_abs
        self.desired_attitude = desired_attitude
        self.sensors = sensors
        self.actuators = actuators
        self.perturbation_noise = perturbation_noise
        self.contr_noise = contr_noise
        self.diag_J = self.J_is_diagonal()
    

    def compute_aerodynamic_torque(self):
        """
        returns the torque that the satellite experiences due to the atmosphere
        """
        altitude = utils.TEME_to_geodetic(*self.r, self.real_time)[2]
        torque = perturbations.aerodynamic_torque(self.r, self.v, altitude, self.attitude,
                                                  self.side_length, self.C_D)
        return torque
    

    def compute_gravity_gradient_torque(self):
        """
        returns the torque that the satellite experiences due to gravity and its attitude
        """
        # if the inertia tensor is a diagonal matrix there is no gravity gradient torque
        if self.diag_J:
            return np.zeros(3)
        return perturbations.gravity_gradient_torque(self.r, self.attitude, self.J)
    

    def compute_solar_radiation_torque(self):
        """
        returns the torque that the satellite experiences due to the radiation from the sun
        """
        return perturbations.solar_radiation_torque(self.r, self.attitude, self.real_time, self.R_spec,
                                                    self.R_diff, self.R_abs, self.side_length)
    
    def compute_magnetic_torque(self):
        """
        returns the torque on the satellite due to the interaction of the magnetic field and
        the magnetic dipole
        """
        return perturbations.magnetic_torque(self.r, self.get_dipole(), self.real_time, self.attitude)


    def compute_pertubation_torques(self):
        """
        wrapper function for all the perturbation torques. returns the total pertubation torque
        """
        aerodynamic_torque = self.compute_aerodynamic_torque()
        gravity_gradient_torque = self.compute_gravity_gradient_torque()
        solar_radiation_torque = self.compute_solar_radiation_torque()
        magnetic_torque = self.compute_magnetic_torque()
        total = np.sum([aerodynamic_torque, gravity_gradient_torque, 
                        solar_radiation_torque, magnetic_torque], axis=0)
        return total
    
    def get_sensor_measurements(self):
        """
        a convenience function that returns the measurements of all on board sensors at the 
        current time step in a dictionary, with key -> name, value -> measurement
        """
        measurements = {}
        for name in self.sensors.keys():
            measurements[name] = self.sensors[name].measure(self)
        return measurements
    
    def get_actuator_controls(self):
        """
        returns the control inputs of the actuators
        """
        controls = {}
        for name in self.actuators.keys():
            controls[name] = self.actuators[name].get_control(self)
        return controls

    def to_sun(self):
        """
        returns a unit vector pointing to the sun in the satellite frame.
        """
        earth_to_sun = environment.earth_to_sun(self.real_time)
        sat_to_sun = np.array(earth_to_sun) - self.r
        sat_to_sun_B = self.attitude.rotate(sat_to_sun)
        return sat_to_sun_B / np.linalg.norm(sat_to_sun_B)
    
    def is_in_shadow(self):
        return environment.is_in_shadow(self.r, self.real_time)

    def J_is_diagonal(self):
        """
        wether the inertia tensor is a diagonal matrix aka it only has nonzero elements
        on the diagonal
        """
        return 0 == np.count_nonzero(self.J - np.diag(np.diag(self.J)))
    
    def get_dipole(self):
        """
        return the total magnetic dipole moments. consist of the residual dipole of the 
        satellite and the dipoles produced by the magnetorquers
        """
        magnetorquers = [self.actuators[name] for name in self.actuators if "mgt" in name]
        actuator_dipoles = np.sum([i.dipole_moment(self) for i in magnetorquers], axis=0)
        return self.residual_dipole + actuator_dipoles
    
    def get_intrinsic_magnetic_field(self, rel_position):
        """
        the magnetic field produced by the magnetorquers and the instrinsic magnetic field
        of the satellite. the sensor is assumed to be outside any of these components
        """
        magnetorquers = [self.actuators[name] for name in self.actuators if "mgt" in name]
        total_field = 0
        #every magnetorquer produces a field at the relative position depending on their 
        #distance and the moment of the torquer
        for mgt in magnetorquers:
            #the vector from the magnetorquer to relative position
            distance_vec = rel_position - mgt.rel_postion
            total_field += utils.dipole_field(distance_vec, mgt.dipole_moment(self))
        #the intrinsic magnetic moment of the satellite also produces a field
        #the dipole for this is assumed to be at the center
        total_field += utils.dipole_field(rel_position, self.residual_dipole)
        return total_field
    