import wmm2020 as wmm
import utils
import numpy as np
import pymap3d as pm
from astropy import coordinates as coord, units as u
from astropy.time import Time

def magnetic_field(position, time):
    """
    returns the magnetic field vector (nT) at the given position(km, TEME frame) and time(julian day)
    refer to https://www.ngdc.noaa.gov/geomag/WMM/soft.shtml for more information
    """
    geo_pos = utils.TEME_to_geodetic(*position, time)
    time_dec = utils.julian_to_decimal(time)
    mag_field = wmm.wmm_point(*geo_pos, time_dec)
    #the wmm returns the coordinates in a north, east, down (NED) frame
    #they are converted to ITRS(whih is an ECEF) and then to the TEME frame
    mag_field_ECEF = pm.ned2ecef(mag_field["north"], mag_field["east"], mag_field["down"],
                                  *geo_pos, pm.Ellipsoid("wgs84"))
    mag_field_TEME = utils.ECEF_to_TEME(*mag_field_ECEF, time)
    #since the TEME and ECEF frames refer to physical points in space above the earth
    #the magnitude of the magnetic field vector in nT gets messed up
    #the magnetic field will always have the same magnitude, independent of the frame
    mag_field_TEME = (mag_field_TEME / np.linalg.norm(mag_field_TEME)) * mag_field["total"]
    return mag_field_TEME

def atmospheric_density(altitude):
    """
    this very simple static model is a first approximation. altitude in km, density in kg/m^3
    a more precise model should be implemented in a final version
    TODO: upgrade model
    """
    # the values are taken from Fundamentals of Spacecraft Attitude Determination and Control
    # by f. Markley and John Crassidis, table D.1
    const = {"p_0":[2.418e-11, 9.158e-12, 3.725e-12, 1.585e-12, 6.967e-13, 1.454e-13, 3.614e-14],
                "h_0":[300, 350, 400, 450, 500, 600, 700],
                "H":[52.5, 56.4, 59.4, 62.2, 65.8, 79, 109]}
    if altitude < 300 or altitude > 800:
        raise Exception("the altitude is outside the range for the atmospheric model.\n" + \
                        "it is:{}, but it should be between 300 and 800".format(altitude))
    else:
        # depending on the altitude a different set of constants needs to be used
        # the altitude should be bigger than h_0 but smaller than the next value of h_0
        i = [j > altitude for j in const["h_0"]].index(True) - 1
    
    return const["p_0"][i] * np.exp(-(altitude - const["h_0"][i])/const["H"][i])

def earth_to_sun(time):
    """
    returns a vector from the earth to the sun in the TEME frame in AU at the given time(julian day)
    """
    time = Time(time, format="jd")
    sun_pos_GCRS = coord.get_sun(time)
    # the vector from earth to the sun in AU in the correct frame
    sun_pos_TEME = sun_pos_GCRS.transform_to(coord.builtin_frames.TEME()).cartesian.xyz
    return sun_pos_TEME

def solar_radiation_pressure(position, time):
    """
    returns the amount of solar radiation pressure at the position. only considers light
    coming directly from the sun.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis Appendix D.3
    """
    sun_pos_TEME = earth_to_sun(time)
    position_AU = (position * u.km).to(u.AU)
    sat_to_sun_AU = np.array(sun_pos_TEME) - np.array(position_AU)
    # speed of light in m/s
    c = 299792458
    # solar constant in W/m^2, flux density of solar radiation at adistance of 1 AU from the Sun
    # can vary up to 5 W/m^2 daily which wont matter for this purpose
    solar_const = 1362
    solar_pressure = solar_const / (c * (np.linalg.norm(sat_to_sun_AU)*1000)**2)
    return solar_pressure

def is_in_shadow(position, time):
    """
    returns a boolean describing wether the satellite is in the shadow 
    of the earth. uses the cylindrical shadow projection.
    from: Fundamentals of Spacecraft Attitude Determination and Control
    by f. Markley and John Crassidis Appendix D.3
    """
    sun_position_unit = earth_to_sun(time) / np.linalg.norm(earth_to_sun(time))
    earth_radius = 6371 #km
    return np.dot(position, sun_position_unit) < - np.sqrt(np.linalg.norm(position)**2 - earth_radius**2)