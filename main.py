from satellite import Satellite
from simulation import simulate_adcs
from orbit import setup_orbit, julian_day_frac
from pyquaternion import Quaternion
import numpy as np
import configparser, plotting, utils

def main(config_file):
    config = configparser.ConfigParser(empty_lines_in_values=False)
    config.read(config_file)

    sgp4_sat = setup_orbit(config)
    # a tuple containing the julian day and the fraction of that day 
    # corresponding to the real time
    start_real_time = julian_day_frac(config)

    if config["satellite"]["inital_attitude"] == "random":
        initial_attitude = Quaternion.random()
    else:
        initial_attitude = utils.read_config_quaternion(config["satellite"]["inital_attitude"])
    
    desired_attitude = utils.read_config_quaternion(config["satellite"]["desired_attitude"])

    omega = utils.read_config_vector(config["satellite"]["omega"])

    # constructing the inertia tensor
    inertia_row_1 = utils.read_config_vector(config["satellite"]["inertia_row_1"])
    inertia_row_2 = utils.read_config_vector(config["satellite"]["inertia_row_2"])
    inertia_row_3 = utils.read_config_vector(config["satellite"]["inertia_row_3"])
    inertia_tensor = np.array([inertia_row_1, inertia_row_2, inertia_row_3])

    residual_dipole = utils.read_config_vector(config["satellite"]["dipole"])

    #constructing the sensors
    sensors = utils.make_sensors(config)
    #constructing the actuators
    actuators = utils.make_actuators(config)

    #constructing the satellite
    sat = Satellite(inertia_tensor, 
                    omega,
                    residual_dipole,
                    float(config["satellite"]["side_length"]), 
                    initial_attitude,
                    desired_attitude,
                    float(config["satellite"]["C_D"]),
                    float(config["satellite"]["R_spec"]),
                    float(config["satellite"]["R_diff"]),
                    float(config["satellite"]["R_abs"]),
                    sensors, actuators,
                    perturbation_noise=float(config["simulation"]["perturbation_noise"]))
    logs = simulate_adcs(sat, sgp4_sat, start_real_time, 
                         stop_sim_time = int(config["simulation"]["duration"]),
                         delta_t = float(config["simulation"]["delta_t"]))

    #plotting
    if bool(int(config["simulation"]["plotting"])):
        plotting.make_plots(config, logs)

    #saving
    if bool(config["simulation"]["save"]):
        utils.saving(logs, config["simulation"]["save"])
    

if __name__ == "__main__":
    main("sim.conf")